import React, { Component } from 'react'

class LikeButton extends Component
{
     state = {
         likes : 0
     }
   
addLikesHandler = () =>
{
    let newLike = this.state.likes +1;
    this.setState({
        likes : newLike
    })
}
    
   render()
   {
       return(
           <div>
               <button onClick={this.addLikesHandler}>{this.state.likes}</button>
               
           </div>
       )
   }
}

export default LikeButton
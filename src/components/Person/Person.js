import React from 'react'
import classes from './Person.module.css';

const person = (props)=>{
 //return   <p>I am a Person and my age is {Math.floor(Math.random() * 30)}</p>
 const rnd = Math.random();
 if(rnd > 0.7 )
 {
     throw new Error("Something Went Wrong");
 }
 return (
  <div className={classes.Person}> 
     <p onClick={props.click}>My name is {props.name} and I work for {props.company}</p>
     <p >{props.children}</p>
     <input type="text" onChange={props.change} value={props.name}></input>
     {/* <LikeButton style={style}></LikeButton> */}
 </div>
)
}

export default person
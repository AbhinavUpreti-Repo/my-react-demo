
import React, { Component } from 'react';
import '../container/App.css';
import Person from '../components/Person/Person';
import styled from 'styled-components';
import ErrorBoundary from '../components/ErrorBoundary/ErrorBoundary'

const StyledButton = styled.button`
border:1px black;
background-color :${props => props.alt? 'red' : 'green'};
padding:8px;
font:inherit;

&:hover{
  background-color: ${props => props.alt? 'salmon' : 'lightgreen'};
  color: black;
}
`;


class App extends Component
{
 state = {
    persons: [
      {id:'asd', name:'Abhinav' , company : 'Accolite'},
      {id:'dfg,',name:'Alok' , company : 'Morgan'},
      {id:'zxc',name:'Nidhi' , company: 'TCS'}
    ],
    ButtonName : "SwitchName",
    showPerson : false
  }
  



//   //Button Click Event 
  

  nameChangeHandler = (event,id) => {
    const personIndex  = this.state.persons.findIndex(p=>{
      return p.id===id;
    });
    const person = {...this.state.persons[personIndex]};
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex]= person;
    this.setState ( {
      persons: persons
    })
    }

    displayPerson = ()=>
    {
      
      const doesShow = this.state.showPerson;
      this.setState({
        showPerson : !doesShow
      })
    }

    deletePersonHandler = (personIndex)=>
    {
     
      const persons = [...this.state.persons];
      persons.splice(personIndex,1);    
      this.setState({persons:persons});

    }

   
render(){
// const style = {
//   border:"1px black",
//   backgroundColor : "green",
//   padding:"8px",
//   font:"inherit",
//   ":hover":{
//     backgroundColor: 'lightgreen',
//     color: 'black'
//   }
// }
let persons = null;
let styleClasses = [];//['red','bold'].join(' ');
if(this.state.persons.length <=2)
{
  styleClasses.push('red');
}
if(this.state.persons.length <=1)
{
  styleClasses.push('bold');
}
if(this.state.showPerson)
{
  persons=(<div>
    {this.state.persons.map((person,index) =>
      {
       return <ErrorBoundary key={person.id}>
        <Person name={person.name} click={()=>this.deletePersonHandler(index)}
        change={(event)=>this.nameChangeHandler(event,person.id)} 
        company={person.company} >
        </Person>
        </ErrorBoundary>
      })
    }
    </div>)
    // style.backgroundColor = "red";
    // style[":hover"] = {
    //   backgroundColor: 'orange',
    //   color: 'black'
    // };
}

  return (
<div className="App">
<h1 >Hi , I am a react App </h1>
<p className={styleClasses.join(' ')}>This is working fine!</p>
<StyledButton alt={this.state.showPerson}  onClick={this.displayPerson}>TogglePersons</StyledButton>



{/* Using a variable inside the render method */}
{persons}

</div>
   // React.createElement('div',{className : 'App'},React.createElement('h1',null,'This is a test Header')))
    //React.createElement('br',null,null))
  )
}
}

export default App